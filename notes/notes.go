package note

import (

	"zulunity.com/database"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
	_ "gorm.io/driver/sqlite"
)


type Note struct {
	gorm.Model
	Title  string `json:"Title"`
	Creator string `json:"Creator"`
	Body string    `json:"Body"`
}

func GetNotes(c *fiber.Ctx) error{
	db := database.DBConn
	var notes []Note
	db.Find(&notes)
	return c.JSON(notes)
}

func GetNote(c *fiber.Ctx) error{
	id := c.Params("id")
	db := database.DBConn
	var note Note
	db.Find(&note,id)
	return c.JSON(note)
}

func NewNote(c *fiber.Ctx) error{
	db := database.DBConn
	note := new(Note)
	if err := c.BodyParser(note); err != nil {
		return c.SendStatus(503)
	}
	db.Create(&note)
	return c.JSON(note)
}

func DeleteNote(c *fiber.Ctx) error{
	id := c.Params("id")
	db := database.DBConn
	
	var note Note
	db.First(&note, id)
	if note.Title == ""{
		return c.Status(500).SendString("No Note found with ID")
	}
	db.Delete(&note)
	return c.SendString("Book Deleted Succesfully")
}
