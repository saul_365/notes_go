package main

import (
	"log"
	"os"

	"zulunity.com/notes"
	"zulunity.com/routes"
	"zulunity.com/database"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
	"gorm.io/driver/sqlite"
)
var (
    port   = os.Getenv("PORT")
)

func init(){
	if port == ""{
		port =":3000"
	}
}

func initDB(){
	var err error
	database.DBConn, err = gorm.Open(sqlite.Open("notes.db"))
	if err != nil{
		log.Fatal("failed to connect database")
	}
	log.Println("Connection Opened to Database")
	database.DBConn.AutoMigrate(&note.Note{})
	log.Println("DB Migrated")
}


func main(){
	app := fiber.New()
	initDB()
	routes.Setup(app) 
	app.Listen(port)
	//defer database.DBConn.Close()
}
