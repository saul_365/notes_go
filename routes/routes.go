package routes

import(
	"github.com/gofiber/fiber/v2"
	"zulunity.com/notes"
)

func Setup(app *fiber.App){
	app.Get("/api/v1/note",note.GetNotes)
	app.Get("/api/v1/note/:id",note.GetNote)
	app.Post("/api/v1/note",note.NewNote)
	app.Delete("/api/v1/note",note.DeleteNote)
}
